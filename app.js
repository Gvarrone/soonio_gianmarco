const hamburgerToggle = document.querySelector('#hamburgerToggle')

const hamburgerIcon = document.querySelector('#hamburgerToggle .fa-solid.fa-ellipsis-vertical')

hamburgerToggle.addEventListener('click', ()=>{
    hamburgerIcon.classList.toggle('fa-rotate-90')
})